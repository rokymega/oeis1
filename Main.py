__author__ = 'Rok_in_Sebastjan'

from GlavnoOkno import *
from ZacetnoOkno import *
from Paketi import *

class Aplikacija():

    def __init__(self):
        self.root = Tk()
        self.init_meni()
        self.bindings()
        self.app = Zacetno(self.root)
        self.root.mainloop()

    def switch(self, *args):
        '''
        Metoda spremlja stanje atributa zamenjaj in v ustreznem primeru inicializira spremembo okna iz začetnega v glavnega
        '''
        if self.app.zamenjaj:
            self.root.destroy()
            self.root = Tk()
            self.app = Glavno(self.root, zaporedje = self.app.vnos)
            self.init_meni()
            self.bindings()

    def novoIskanje(self, *args):
        '''
        Metoda, ki pripravi novo začetno okno
        '''
        self.root.destroy()
        self.root = Tk()
        self.app = Zacetno(self.root)
        self.init_meni()
        self.bindings()

    def bindings(self):
        '''
        Vezave tipk
        '''
        self.root.bind('<ButtonRelease-1>',self.switch)
        self.root.bind('<KeyRelease-Return>', self.switch)


    def init_meni(self, *args):
        '''
        Konfiguracija orodne vrstice
        '''
        meni = Menu(self.root)
        self.root.config(menu = meni)
        subMeni = Menu(meni)
        meni.add_cascade(label = "Iskalnik zaporedij", menu = subMeni)
        subMeni.add_command(label = "Novo iskanje", command = self.novoIskanje)
        subMeni.add_separator()
        subMeni.add_command(label = "O avtorjih", command = self.oAvtorjih)


    def oAvtorjih(self, *args):
        messagebox.showinfo(title = "Avtorja", message = avtorja)



app = Aplikacija()
