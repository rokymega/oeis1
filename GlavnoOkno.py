__author__ = 'Rok_in_Sebastjan'



from Paketi import *
# from havlasfunkcije import *


test = OrderedDict([("Členi zaporedja","1,2,3,4,5,6,7,8,9..."), ("Reference","Test, Test"), ("Druga zaporedja",["A123456","A987654","A132456", "A253918","A123456","A987654","A132456", "A253918","A123456","A987654","A132456", "A253918"]),
                      ("Test",['a', 'b', 'c'])])

class Glavno():

    def __init__(self, root, zaporedje = "A123477\nA123456"):
        #
        #   Atributi, ki so prikazujejo vsebujejo vsebinski tekst in pa parametre za spremembo oken
        #
        try:
            self.slovar = havlasfn(zaporedje)
        except:
            self.slovar = test


        self.zamenjaj = False
        self.vnos = StringVar



        root.title("Zaporedje: {}".format(zaporedje))
        root.configure(background = barva_ozd)

        #
        #   Konfiguracija in osnovna postavitev
        #

        zgOkv = Frame(root)
        spOkv = Frame(root)
        zgOkv.pack(side = TOP, fill = BOTH, expand = True)
        spOkv.pack(side = BOTTOM, fill = BOTH, expand = True)
        zgOkv.configure(background = barva_ozd)
        spOkv.configure(background = barva_ozd)

        #
        #   Zgornji okvir
        #

        ime_zap = ttk.Label(zgOkv, text = "A123456\nA123456", font = ("Helvetica", 22))
        ime_zap.pack(side = LEFT, fill = X)

        #
        #   Spodnji okvir
        #

        zvezek = ttk.Notebook(spOkv)
        zvezek.grid(row = 0, column = 0)

        for ime, vsebina in self.slovar.items():
            tab = ttk.Frame(zvezek)
            self.txt_wndw(tab ,ime , vsebina)
            zvezek.add(tab, text = ime)

    def txt_wndw(self, parent, ime,  vsebina):

            '''
            :param parent: staševski widget v katerem je vsebovano tekstovno okno
            :param ime: ime zavihka
            :param vsebina: tekst
            Metoda konfigurira tekstovno okno z vertikalnim drsnikom na desnem robu, v posebnem primeru pa
            doda še hiperpovezave, ki poženejo novo iskalno okno.
            '''
            vsb = Scrollbar(parent)
            vsb.pack(side = "right", fill = "y")

            w = Text(parent, yscrollcommand = vsb.set, font = ("Helvetica", 16), width = 50, height = 10)
            w.pack()

            if ime.lower() == "druga zaporedja":

                hl = HyperlinkManager(w)

                def drugo_zap(*args):
                    '''
                    Funkcija, ki ob kliku na hiperpovezavo sproži novo glavno okno z izbranim zaporedjem
                    '''

                    indeks = hl.text.tag_names(CURRENT)[1][-1]
                    #
                    #   hl.text.tag_names(CURRENT) vrne par ('hyper', 'hyper-število')
                    #
                    self.vnos = self.slovar["Druga zaporedja"][eval(indeks)]
                    self.zamenjaj = True
                    return

                for z in vsebina:
                    w.insert(INSERT, "- ")
                    w.insert(INSERT, z, hl.add(drugo_zap))
                    w.insert(INSERT,'\n')

            else:
                w.insert(INSERT, vsebina)

            w.config(state = DISABLED)
            vsb.config(command = w.yview)
