__author__ = 'sebastjancizel'


from tkinter import *
from tkinter import ttk
from random import randint
from tkinter import messagebox
from time import sleep
from collections import OrderedDict

barva_ozd = "#DEDEDE"


navodilo = '''Vpišite zaporedje v obliki števil ločenih z vejico (primer vnosa: 1,2,3,4):'''
naslov = '''ISKALNIK ZAPOREDIJ V BAZI OEIS'''
avtorja = "Avtorja tega projekta sta Rok Havlas in Sebastjan Cizel."



class HyperlinkManager(object):
    """
    Razred za dodajanje hiperpovezav v tekstovni widget.
    Koda izposojena iz: http://effbot.org/zone/tkinter-text-hyperlink.htm
    """
    def __init__(self, text):
        self.text = text
        self.text.tag_config("hyper", foreground="blue", underline=1)
        self.text.tag_bind("hyper", "<Enter>", self._enter)
        self.text.tag_bind("hyper", "<Leave>", self._leave)
        self.text.tag_bind("hyper", "<Button-1>", self._click)
        self.reset()

    def reset(self):
        self.links = {}

    def add(self, action):
        """Adds an action to the manager.
        :param action: A func to call.
        :return: A clickable tag to use in the text widget.
        """
        tag = "hyper-%d" % len(self.links)
        self.links[tag] = action
        return ("hyper", tag)

    def _enter(self, event):
        self.text.config(cursor="hand")

    def _leave(self, event):
        self.text.config(cursor="")

    def _click(self, event):
        for tag in self.text.tag_names(CURRENT):
            if (tag[:6] == "hyper-"):
                self.links[tag]()
                return
